-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 24. Sep 2011 um 16:31
-- Server Version: 5.1.58
-- PHP-Version: 5.3.8-1~dotdeb.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `osubm`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beatmap_downloads`
--

CREATE TABLE IF NOT EXISTS `beatmap_downloads` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL DEFAULT '1',
  `packid` int(9) NOT NULL,
  `themeid` int(9) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `size` bigint(14) NOT NULL,
  `ip` bigint(10) NOT NULL DEFAULT '2130706433',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38606 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beatmap_maps`
--

CREATE TABLE IF NOT EXISTS `beatmap_maps` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `packid` int(5) NOT NULL DEFAULT '1',
  `mapid` int(8) NOT NULL,
  `map` varchar(250) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `filesize` int(25) NOT NULL,
  `downloads` int(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5450 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beatmap_packs`
--

CREATE TABLE IF NOT EXISTS `beatmap_packs` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `themeid` int(5) NOT NULL DEFAULT '1',
  `packnum` int(4) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `uploader` varchar(40) NOT NULL,
  `beatmaps` int(3) NOT NULL,
  `size` int(25) NOT NULL,
  `downloads` int(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=428 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beatmap_themes`
--

CREATE TABLE IF NOT EXISTS `beatmap_themes` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `theme` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
