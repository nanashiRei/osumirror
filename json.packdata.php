<?php

include 'inc/config.php';

$query = "
	SELECT 
		`beatmap_packs`.*,
		`beatmap_themes`.`theme`
	FROM 
		`beatmap_packs`,
		`beatmap_themes`
	WHERE
		`beatmap_packs`.`themeid` = `beatmap_themes`.`id`
		AND `beatmap_packs`.`packnum` = %d
		AND `beatmap_packs`.`themeid` = %d 
	ORDER BY
		`beatmap_themes`.`id` ASC, 
		`beatmap_packs`.`packnum` ASC";

if(empty($_GET['themeId']) || empty($_GET['packNum']))
    die(json_encode(array('result'=>'error','error'=>'Missing arguments!')));

if(!is_numeric($_GET['themeId']))
{
    $idRes = $m->query("SELECT `id` FROM `beatmap_themes` WHERE `theme` = '" . $m->real_escape_string(str_replace('_',' ',$_GET['themeId'])) . "'");
    if($idRes->num_rows)
    { $_GET['themeId'] = $idRes->fetch_object()->id; }
    else
    { die(json_encode(array('result'=>'error','error'=>'Theme could not be parsed into an ID'))); }
}
    
$packRes = $m->query(sprintf($query,$_GET['packNum'],$_GET['themeId']));

if($packRes->num_rows)
{
    $packData = $packRes->fetch_assoc();
    $dlHash = hash('sha1',ip2long($_SERVER['REMOTE_ADDR']).DOWNLOAD_SALT);
    //$packData['downloadLink'] = '/fetch/' . $dlHash . bin2hex(base64_encode('/packs/'.($packData['themeid'] == 1 ? 'default' : $packData['theme']).'/'.$packData['filename'])) . '?type=1&data=' . urlencode(serialize($packData)) . '&.jdeatme';
    $packData['downloaded'] = $userDL[$packData['theme']][$packData['packnum']];
    $packData['downloadLink'] = '/fetch/' . $dlHash . '-p-' . $packData['themeid'] . '-' . $packData['packnum'] . '.rar';
    $mapListRes = $m->query("SELECT * FROM `beatmap_maps` WHERE `packid` = " . $packData['id']);
    $maps = array();
    while($map = $mapListRes->fetch_assoc())
    {
        $maps[$map['mapid']] = $map;
    }
    $packData['maps'] = $maps;
    
    echo json_encode(array(
        'result' => 'success',
        'success' => $packData
    ));
}
else
{
    die(json_encode(array('result'=>'error','error'=>'Theme or pack does not exist!')));
}     
?>