<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/CreativeWork" xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <!-- Meta Info -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="author" content="David Marner" />
    <meta name="keywords" content="games, yas, osu, yet another server, fun, neic0, nanashirei, web" />
    <meta name="description" content="osu! Beatmap Pack Mirror" />
    
    <meta itemprop="name" content="osu! Beatmap Pack Mirror">
    <meta itemprop="description" content="osu! beatmap packs and maps download mirror">
    <meta itemprop="image" content="http://osu.yas-online.net/images/mirrorlogo.png">
    
    <meta property="og:title" content="osu! Beatmap Pack Mirror" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://osu.yas-online.net" />
    <meta property="og:image" content="http://osu.yas-online.net/images/mirrorlogo.png" />
    <meta property="og:site_name" content="osu! Beatmap Pack Mirror" />
    <meta property="fb:admins" content="1351130029" />
    
    <link rel="dns-prefetch" href="//osu.api.yas-online.net" />
    <link rel="dns-prefetch" href="//dl2.osupacks.yas-online.net" />
    <link rel="dns-prefetch" href="//dl3.osupacks.yas-online.net" />
    <link rel="dns-prefetch" href="//osumirror.ezoweb.net" />
    <link rel="dns-prefetch" href="//osu-mirror.casiobeatz.name" />
    
    <title>osu! Beatmap Packs</title>
    
    <!-- Stylesheets and style related -->
    <link type="text/css" rel="stylesheet" id="page_stylesheet" href="/css/{$smarty.cookies.style}" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/png" />

    <!-- Scripting -->
    <script src="/lib/language.js.php{if !empty($smarty.get.l)}?l={$smarty.get.l}{/if}" type="text/javascript"></script>
    
    <script src="/min/f=lib/jquery.js,lib/jquery.cookie.js,zc/ZeroClipboard.js,lib/mirror.js" type="text/javascript"></script>
    
    <!-- Stuff that gets appended by scripts -->
</head>