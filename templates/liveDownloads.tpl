{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script src="/lib/livedownloads.js" type="text/javascript"></script>

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        {include 'stats_navigation.tpl'}
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody stats_pages">
        <div id="packInfo">
            <div class="maplist">
                <table id="downloads" class="maptable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th stlye="width: 6px;"></th>
                            <th>
                                Device / Platform &amp;<br />
                                <small>IP / Ident</small>
                            </th>
                            <th>Type</th>
                            <th>ID</th>
                            <th>Size</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}