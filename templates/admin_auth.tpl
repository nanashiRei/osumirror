{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script type="text/javascript">
$(document).ready(function(){
    $('table.maptable tbody tr:nth-child(even)').addClass('even');
});
</script>
<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        {include 'stats_navigation.tpl'}
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody stats_pages">
        <div id="packTitle">
            <h1>Control Panel Access</h1>
        </div>
        <div id="packInfo">
            <form action="" method="post">
                <input type="password" name="passkey" /> <input type="submit" value="Verify" />
                <input type="hidden" name="_auth_me" value="mir-1-admin" />
            </form>
        </div>
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}