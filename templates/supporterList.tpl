{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script type="text/javascript">
$(document).ready(function(){
    $('.supporter').click(function(){
        var banner = 'http://osu.ppy.sh/stat2/' + $(this).attr('data-key') + '-' + $(this).attr('data-gamemode') + '.png';
        $('.supporter_info h2').text($(this).attr('data-key'));
        $('.supporter_info .supporter_occupation').text($(this).attr('data-occupation'));
        $('.supporter_info .user_banner img').attr('src',banner);
        $('.supporter_info .user_banner a').attr('href','http://osu.ppy.sh/u/' + $(this).attr('data-id'));
        $('.supporter').removeClass('sup-active');
        $(this).addClass('sup-active');
        location.hash = $(this).attr('data-key').replace(/ /g,'+');
    });
    $('.supporter').hover(function(){ $(this).addClass('sup-hover'); },function(){ $(this).removeClass('sup-hover'); });
    
    var jump = location.hash.substr(1).replace(/\+/g,' ');
    if(jump){
        var supporters = $('.supporter[data-key="' + jump + '"]');
        if(supporters.length > 0)
            supporters.click();
        else
            $('.supporter:first').click();
    }else 
        $('.supporter:first').click();
});
</script>
<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        <div class="info">
            {$language.support_side_note_1}
        </div>
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div class="donation_notice">
            {$language.support_side_note_2}

            <div style="text-align: center;">
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="839T4J92KW244">
                <!--<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
                <input type="submit" value="Donate now!" />
                <img alt="" border="0" src="https://www.paypalobjects.com/de_DE/i/scr/pixel.gif" width="1" height="1">
                </form>
            </div>
        </div>
        <br />
        
        <div class="supporter_info">
            <h2>nanashiRei</h2>
            <div class="supporter_occupation">Webmaster</div>
            <div class="user_banner">
                <a href="" target="osu"><img src="" alt="userbanner" /></a>
            </div>
        </div>
        <br />
        
        <div class="supporter_list_box" style="overflow: hidden;">
            <ul class="supporter_list">
            {foreach $Supporters as $Supporter}
                <li class="supporter" data-key="{stripslashes($Supporter@key)}" data-occupation="{$Supporter.occupation}" data-id="{$Supporter.id}" data-gamemode="{$Supporter.gamemode}">
                    <strong>{stripslashes($Supporter@key)}</strong> <span>{$Supporter.occupation}</span>
                </li>
            {/foreach}
            </ul>
        </div>
    </div>
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}