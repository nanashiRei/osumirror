{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        {include 'stats_navigation.tpl'}
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody stats_pages">
        <div id="packTitle">
            <h1>Download Link Tester</h1>
        </div>
        <div id="packInfo">
            <form method="post" action="/stats/dltester/">
                <select name="mirror">
                    {foreach $Mirrors as $Mirror}
                        <option value="{$Mirror@key}" {if $SelectedMirror == $Mirror.host}selected="selected"{/if}>{$Mirror.host}</option>
                    {/foreach}
                </select>
                <select name="packnum">
                    {foreach $BeatmapPacks as $Pack}
                        <option value="{$Pack.pack_num}" {if $smarty.post.packnum == $Pack.pack_num}selected="selected"{/if}>BMP #{$Pack.pack_num}</option>
                    {/foreach}
                </select>
                <input type="submit" value="Make ticket link..." />
            </form>
            <br />
            <b>Old Method (still used!) (Length: {strlen($data)}bytes)</b><br />
            <textarea style="width: 600px; height: 140px;">http://{$SelectedMirror}/api/get_download/?request={$data}</textarea>
            <br />
            <b>New Method (not implemented) (Length: {strlen($deflated_data)}bytes)</b><br />
            <textarea style="width: 600px; height: 140px;">http://{$SelectedMirror}/api/download/{$deflated_data}</textarea>
            <br />
            <a href="http://{$SelectedMirror}/api/get_download/?request={$data}">Click to start download test</a>
        </div>
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}