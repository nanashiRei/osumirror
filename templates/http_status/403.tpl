{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div id="packTitle"><h1>Access Denied!</h1></div>
        <div id="errorMessage">You may not access this resource!</div>       
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
</body>
</html>