{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div id="packTitle"><h1>File not found!</h1></div>
        <div id="errorMessage">The file you have request could not be found on this server!</div>       
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
</body>
</html>
