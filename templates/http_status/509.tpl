{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div id="packTitle"><h1>Bandwidth Limit Reached</h1></div>
        <div id="errorMessage">Please slow down a bit! You have downloaded {$smarty.request.downloaded}.
        The Limits are as follows: 1000MB in 30 minutes</div>       
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
</body>
</html>
