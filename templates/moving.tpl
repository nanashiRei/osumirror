{if $smarty.server.HTTP_HOST == "osupacks.ppy.sh"}
    <div class="donation_notice" style="border: 2px solid #ff5d5d; margin-bottom: 5px; text-align: center; font-size: 11pt;">
        Hi! We are moving to another domain! <a style="color: #ffffff;" href="http://osu.yas-online.net{$smarty.server.REQUEST_URI}">osu.yas-online.net</a>
        please update your bookmark. The old domain 'osupacks.ppy.sh' will go offline sometime
        soon.
    </div>
{/if}