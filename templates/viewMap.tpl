{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script src="/lib/map.loader.js" type="text/javascript"></script>

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        <div class="info">
            {$language.maps_side_note_1}
        </div>
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div id="packTitle"><h1>#<strong>---</strong> - <span id="mapName">---</span></h1></div>
        <div id="packActions">
            <a id="downloadButton" class="downloadbutton" href="" onclick="setTimeout('jumpNavHandle();',3000);">{$language.maps_download_button} (<span id="packSize">-- MB</span>)</a>
            <a id="copyInGameButton" class="copybutton" href="javascript:;">{$language.beatmap_copy_ingame_button}</a>
        </div>
        <div id="packInfo">Loading...</div>
        <div id="packInfoTemplate">
            <div class="infobar">
                <table class="noformat">
                    <tbody>
                        <tr>
                            <td>{$language.beatmap_label_downloads}: <span id="downloadCount">--</span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>{$language.beatmap_label_filename}: <span id="fileName">--</span></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <h2>{$language.maps_label_packs_that_contain_this_map}</h2>
            <div class="maplist">
                <table class="maptable">
                    <thead>
                        <tr>
                            <th style="width: 50px;">{$language.maps_packlist_id}</th>
                            <th>{$language.maps_packlist_theme}</th>
                            <th style="width: 50px;">{$language.maps_packlist_size}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {include 'disclaimer.tpl'}
        <div style="text-align: right;">
            <img src="/images/yasproject.jpg" alt="YaS Code" />
            <img src="/images/jspowered.jpg" alt="Javascript Powered" />
            <img src="/images/xenvirtual.jpg" alt="XEN Virtualized" />
        </div>
    </div>
    <br class="clear" />
    
</div>

{include 'noscript.tpl'}
{include 'foot.tpl'}