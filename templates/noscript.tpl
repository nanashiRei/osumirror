<noscript>
    <div class="noscriptOverlay">
        <div class="noscriptBox">
            <h1 style="color: black;">You have to enable Javascript for this page to work!</h1>
        </div>
    </div>
</noscript>