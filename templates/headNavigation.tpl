<!-- Navigation -->
<div class="navigation">
    <ul>
        <li><a href="/p">{$language.navigation_beatmap_packs}</a></li>
        <li><a href="/t">{$language.navigation_theme_packs}</a></li>
        <li><a class="maps" href="/m">{$language.navigation_maps}</a></li>
        <li><a class="search" href="/f">{$language.navigation_search}</a></li>
        <li><a class="supporters" href="/supporter">{$language.navigation_supporters}</a></li>
    </ul>
    <br />
</div>