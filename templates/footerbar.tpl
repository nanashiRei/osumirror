<div class="footerbar">
	<div class="footerbar-inner">
        
        <div class="more">
		    <ul>   
		        <li><a href="http://blog.mhistory.info">MH</a></li>
		        <li><a href="http://neic0.de">Neico</a></li>
		        <li><a href="http://osu.ppy.sh">osu!</a></li>
		        <li><a href="http://nanashirei.me">nanashiRei.me</a></li>
		        <li>osu!Mirror &copy; 2011 - 2012 David Marner</li>
		    </ul>
	    </div>
	    
        <!-- AddThis Button BEGIN --> 
        <div class="addthis_toolbox addthis_default_style"
        	addthis:url="http://osu.yas-online.net"
	        addthis:title="osu!Mirror - download Beatmaps and Packs for osu! via @osumirror"
	        addthis:description="Download Beatmaps and Packs for osu!">
            <a class="addthis_button_twitter"></a>
            <a class="addthis_button_google"></a>
            <a class="addthis_button_facebook"></a>
            <a class="addthis_button_email"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4fa6785569fd5b6e"></script>
        <!-- AddThis Button END -->
	    
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('.footerbar li').hover(function(){ $(this).addClass('foot-hover'); },function(){ $(this).removeClass('foot-hover'); });
    $('.footerbar').hover(function(){ $(this).stop().fadeTo(400,.8); },function(){ $(this).stop().fadeTo(400,.3); });
});
</script> 