{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script src="/lib/pack.jumpnav.js" type="text/javascript"></script>

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        {foreach $packList as $packSection}
            <a id="sectionButton" href="javascript:;" onclick="sectionOpen(this);">@<strong>{$packSection@key + 100}</strong></a>
            <div id="packBlockSection">
                {foreach $packSection as $pack}
                    {if $pack@first || $pack.packnum % 100 == 25 || $pack.packnum % 100 == 50 || $pack.packnum % 100 == 75}
                        <a id="subSectionButton" href="javascript:;" onclick="subSectionOpen(this);">#<strong>{$pack.packnum}</strong></a>
                        <div id="packSection">
                        <ul>
                    {/if}
                    <li><a id="pack{$pack.packnum}" class="{if isset($UserDL.{$pack.theme}.{$pack.packnum})}downloaded{/if}" href="javascript:;">#<strong>{$pack.packnum}</strong> <small>{$pack.beatmaps} {$language.beatmap_maps}</small></a></li>
                    {if $pack@last || $pack.packnum % 100 == 26 || $pack.packnum % 100 == 51 || $pack.packnum % 100 == 76}
                        </ul>
                        </div>
                    {/if}
                {/foreach}
            </div>
        {/foreach}
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        {include 'moving.tpl'}
        <div id="packTitle"><h1>{$language.beatmap_pack_default} #<strong>---</strong></h1></div>
        <div id="packActions">
            <a id="downloadButton" class="downloadbutton" href="" onclick="setTimeout('jumpNavHandle(); update_stats();',1500); $('#pack'+$('#packTitle h1 strong').text()).addClass('downloaded');">{$language.beatmap_download_button} (<span id="packSize">-- MB</span>)</a>
            <a id="copyInGameButton" class="copybutton" href="javascript:;">{$language.beatmap_copy_ingame_button}</a>
        </div>
        <div id="packInfo">Loading...</div>
        <div id="packInfoTemplate">
            <div class="infobar">
                <table class="noformat">
                    <tbody>
                        <tr>
                            <td>{$language.beatmap_label_downloads}: <span id="downloadCount">--</span></td>
                            <td>{$language.beatmap_label_filename}: <span id="fileName">--</span></td>
                        </tr>
                        <tr>
                            <td>{$language.beatmap_label_upload}: <a href="/supporter"><span id="uploader">--</span></a></td>
                            <td>{$language.beatmap_label_download_status}: <span id="downloadStatus">--</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <h2>{$language.beatmap_label_maps_in_this_pack}</h2>
            <div class="maplist">
                <table class="maptable">
                    <thead>
                        <tr>
                            <th style="width: 50px;">{$language.beatmap_maplist_id}</th>
                            <th>{$language.beatmap_maplist_map}</th>
                            <th style="width: 50px;">{$language.beatmap_maplist_size}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {include 'disclaimer.tpl'}
        <div style="text-align: right;">
            <img src="/images/yasproject.jpg" alt="YaS Code" />
            <img src="/images/jspowered.jpg" alt="Javascript Powered" />
            <img src="/images/xenvirtual.jpg" alt="XEN Virtualized" />
        </div>
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}