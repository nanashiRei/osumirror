{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script src="/lib/themepack.jumpnav.js" type="text/javascript"></script>

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        {foreach $packList as $packTheme}
            <a id="subSectionButton" href="javascript:;" onclick="subSectionOpen(this);"><strong>{$packTheme@key}</strong></a>
            <div id="packSection">
                <ul>
                    {foreach $packTheme as $pack}
                        <li><a id="pack{$pack.theme|replace:" ":"_"}-{$pack.packnum}" href="javascript:;" class="{if isset($UserDL.{$pack.theme}.{$pack.packnum})}downloaded{/if}">#<strong>{$pack.packnum}</strong> <small>{$pack.beatmaps} {$language.beatmap_maps}</small></a></li>
                    {/foreach}
                </ul>
            </div>
        {/foreach}
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div id="packTitle"><h1><span id="themeTitle">Theme</span> #<strong>---</strong></h1></div>
        <div id="packActions">
            <a id="downloadButton" class="downloadbutton" href="" onclick="setTimeout('jumpNavHandle(); update_stats();',1500); $('#pack'+$('#packTitle #themeTitle').text()+'-'+$('#packTitle h1 strong').text()).addClass('downloaded');">{$language.beatmap_download_button} (<span id="packSize">-- MB</span>)</a>
            <a id="copyInGameButton" class="copybutton" href="javascript:;">{$language.beatmap_copy_ingame_button}</a>
        </div>
        <div id="packInfo">Loading...</div>
        <div id="packInfoTemplate">
            <div class="infobar">
                <table class="noformat">
                    <tbody>
                        <tr>
                            <td>{$language.beatmap_label_downloads}: <span id="downloadCount">--</span></td>
                            <td>{$language.beatmap_label_filename}: <span id="fileName">--</span></td>
                        </tr>
                        <tr>
                            <td>{$language.beatmap_label_upload}: <a href="/supporter"><span id="uploader">--</span></a></td>
                            <td>{$language.beatmap_label_download_status}: <span id="downloadStatus">--</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <h2>{$language.beatmap_label_maps_in_this_pack}</h2>
            <div class="maplist">
                <table class="maptable">
                    <thead>
                        <tr>
                            <th style="width: 50px;">{$language.beatmap_maplist_id}</th>
                            <th>{$language.beatmap_maplist_map}</th>
                            <th style="width: 50px;">{$language.beatmap_maplist_size}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {include 'disclaimer.tpl'}
        <div style="text-align: right;">
            <img src="/images/yasproject.jpg" alt="YaS Code" />
            <img src="/images/jspowered.jpg" alt="Javascript Powered" />
            <img src="/images/xenvirtual.jpg" alt="XEN Virtualized" />
        </div>
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}