<!-- Header -->
<div class="shadow"></div>
<div class="headline_bg"></div>

<!-- LEFT WIDGETS -->
<div class="themepicker">
    <ul>
        <li><a href="javascript:;" style="background: #fbfbfb;" id="themed_mix" onclick="choose_theme('mix'); return false;"></a></li>
        <li><a href="javascript:;" style="background: #008eca;" id="themed_blu" onclick="choose_theme('blu'); return false;"></a></li>
        <li><a href="javascript:;" style="background: #ffba7a;" id="themed_orange" onclick="choose_theme('orange'); return false;"></a></li>
        <li><a href="javascript:;" style="background: #f4c5d9;" id="themed_pink" onclick="choose_theme('pink'); return false;"></a></li>
        <li><a href="javascript:;" style="background: #c10000;" id="themed_red" onclick="choose_theme('red'); return false;"></a></li>
        <li><a href="javascript:;" style="background: #c4ff7e;" id="themed_green" onclick="choose_theme('green'); return false;"></a></li>
    </ul>
</div>

<div class="wrapper">
<div class="contentbackground"></div>
<div class="headline">
    <div class="logo"></div>
    <h1>{$language.page_title}</h1>
    <p class="stats">[ Asking google about stuff, hold on D: ]</p>
    <div class="languages">
        {foreach $languages as $l}
            <img src="/images/lang/{$l@key}.png" alt="{$l@key}" title="{strtoupper($l@key)}" />
        {/foreach}
    </div>
</div>