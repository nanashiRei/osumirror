{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script type="text/javascript">
$(document).ready(function(){
    $('table.maptable tbody tr:nth-child(even)').addClass('even');
});
</script>
<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        {include 'stats_navigation.tpl'}
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody stats_pages">
        <div id="packTitle">
            <h1>Mirror List</h1>
        </div>
        <div id="packInfo">
            <div class="maplist">
                <table class="maptable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Host</th>
                            <th>Speed</th>
                            <th>Hits</th>
                            <th>Traffic (MB)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $Mirrors as $Mirror}
                            <tr>
                                <td style="text-align: right;">#{$Mirror.id}</td>
                                <td>{if $Mirror.online && $Mirror.active}<span style="color: #91de30;">ON</span>{else}<span style="color: #e74343;">OFF</span>{/if} - {$Mirror.host}</td>
                                <td>{sprintf('%0.2f MB/s',$Mirror.network_speed/8)}</td>
                                <td>{$Mirror.network_hits}</td>
                                <td>{sprintf('%0.1f',$Mirror.network_traffic/pow(1024,2))}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}