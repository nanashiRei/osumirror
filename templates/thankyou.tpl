{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        <div class="info">
            {$language.support_side_note_1}
        </div>
        <div class="info">
            {$language.support_side_note_2}
            <div style="text-align: center;">
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="839T4J92KW244">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.paypalobjects.com/de_DE/i/scr/pixel.gif" width="1" height="1">
                </form>
            </div>
        </div>
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div id="packTitle">
            <h1>Thank you!</h1>
        </div>
        <div id="packInfo">
            You just made sure that i can keep up this project, which has about 250000 active users.
            One of them is you!
            <br />
            <br />
            For that i would like to thank you from the bottom of my heart!
        </div>
    </div>
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}