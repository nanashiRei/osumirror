{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script type="text/javascript" src="/lib/search.js"></script>

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel">
        
    </div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div id="searchBar">
            <form action="/f/" id="searchForm" method="get">
                <input type="text" name="searchQueryString" id="searchQueryString" />
            </form>
        </div>
    </div>
    
</div>
{include 'noscript.tpl'}
{include 'foot.tpl'}