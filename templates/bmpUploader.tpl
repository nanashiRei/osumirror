{include 'head.tpl'}
<body>
{include 'heading.tpl'}
{include 'headNavigation.tpl'}
<!-- Site Specific JS -->
<script src="/lib/bmpu.js" type="text/javascript"></script>

<!-- Content Wrapper -->
<div class="contentwrapper">
    
    <!-- Left Side Panel -->
    <div class="leftpanel"></div>
    
    <!-- Actual Content -->
    <div class="contentbody">
        <div id="packInfo">
            <form id="noSubmit">
                <table class="noformat">
                    <tbody>
                        <tr>
                            <td style="width: 100px;">Theme</td>
                            <td><input type="text" id="themeTitle" placeholder="Theme (blank for default)" /></td>
                        </tr>
                        <tr>
                            <td>You are?</td>
                            <td><input type="text" id="uploader" placeholder="Nickname" /></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div id="packInfo">
            <div id="bmpuploader"></div>
        </div>
    </div>
    <br class="clear" />
    
</div>
{include 'noscript.tpl'}
</body>
</html>