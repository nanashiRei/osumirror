<?php

require 'Smarty.class.php';
    
$smrt = new Smarty();

$smrt->template_dir = 'templates';
$smrt->compile_dir = 'templates/comp';
$smrt->cache_dir = 'templates/cache';

$smrt->loadFilter('output', 'trimwhitespace');
$smrt->caching = false;
$smrt->compile_check = true;
$smrt->force_compile = false;
$smrt->debugging = false;

include dirname(__FILE__).'/language.php';

$smrt->assign('languages',$LANG);
$smrt->assign('language',$LANG[$lang]);

?>