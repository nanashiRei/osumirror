<?php

function mirror_has_map_v1($mirror,$map)
{
    global $log;

    $url = 'http://'.$mirror['host'].'/api/has_map/?args[]='.rawurlencode($map);
    $log->add('API Call: '.$url,FileLog::LogInfo);
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
    curl_setopt($ch,CURLOPT_HEADER,false);
    curl_setopt($ch,CURLOPT_HTTPGET,true);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,20);

    $json_raw = curl_exec($ch);
    $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

    curl_close($ch);

    if($json_raw && $http_code == 200)
    {
        $json_raw = @file_get_contents($url);
        $api_response = @json_decode($json_raw);
        if(isset($api_response->result) && $api_response->result == 'success')
        {
            if(!$api_response->success->{$map})
                $log->add('API Call failed for "'.$map.'" on #'.$mirror['id'].'/'.$mirror['host'],FileLog::LogWarn);
            return $api_response->success->{$map};
        }
    }
    return false;
}

function mirror_has_pack_v1($mirror,$packfile,$theme="default")
{
    global $log;

    if($theme == 'Beatmap Pack') $theme = 'default';
    $url = 'http://'.$mirror['host'].'/api/has_pack/?theme='.rawurlencode($theme).'&args[]='.rawurlencode($packfile);
    $log->add('API Call: '.$url,FileLog::LogInfo);
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
    curl_setopt($ch,CURLOPT_HEADER,false);
    curl_setopt($ch,CURLOPT_HTTPGET,true);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,20);

    $json_raw = curl_exec($ch);
    $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

    curl_close($ch);

    if($json_raw && $http_code = 200)
    {
        $api_response = @json_decode($json_raw);
        if(isset($api_response->result) && $api_response->result == 'success')
        {
            if(!$api_response->success->{$packfile})
                $log->add('API Call failed for "'.$packfile.'" on #'.$mirror['id'].'/'.$mirror['host'],FileLog::LogWarn);
            return $api_response->success->{$packfile};
        }
    }
}