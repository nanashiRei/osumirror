<?php

$default = 'en';
$lang = @substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
if(empty($lang)) $lang = $default;
$locales = dirname(__FILE__).'/../locales/';

if(file_exists($locales.$default.'.ini')){
    $LANG[$default] = parse_ini_file($locales.$default.'.ini');
}else{
    die('Default locale ' . $default . ' could not be loaded!');
}

if(!empty($_GET['l'])) $lang = $_GET['l'];

if(empty($lang)) $lang = $default;

$isfound = false;

$loc = dir($locales);
while($locale_ini = $loc->read())
{
    if($locale_ini[0] == '.') continue;
    $isolang = substr($locale_ini,0,2);
    if($lang == $isolang) $isfound = true;
    if($isolang == $default) continue;
    $locale = parse_ini_file($locales . $locale_ini);
    $LANG[$isolang] = $locale + $LANG[$default];
}

if(!$isfound) $lang = $default;

?>