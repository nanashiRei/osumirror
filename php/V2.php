<?php

class OsuMirror_Api_V2
{
    protected $_key;
    protected $_host;
    protected $_apiMessage;
       
    public function __construct($host,$key)
    {
        $this->_key = $key;
        $this->_host = $host;    
    }
    
    public function apiCall($action,$params)
    {
        $callUrl = 'http://' . $this->_host . '/api/' . $action . '/';
        if(is_array($params)) {
            foreach($params as $param)
                $callUrl .= rawurlencode($param) . '/';
            $callUrl = rtrim($callUrl,'/');
        } else {
            $callUrl .= rawurlencode($params);
        }
        $response = $this->_curlGet($callUrl);
        if($response) {
            $apiResponse = $this->_parseResponse($response);
            if($apiResponse && $apiResponse['status'] == 'ok') {
                return true;
            }
        }
        return false;
    }
    
    public function getApiMessage()
    {
        return $this->_apiMessage;
    }
    
    public function makeTicket($data) 
    {
        $data = json_encode($data);
        $data = $this->_compressStr($data);
        return $this->_encryptData($data);
    }

    protected function _compressStr($data)
    {
        return gzdeflate($data,9);
    }
    
    protected function _encryptData($data)
    {
        $output = $this->_encrypt($data,$this->_key);
        return bin2hex($output);
    }
    
    protected function _encrypt($data,$key)
    {
        $cipherText = mcrypt_encrypt(MCRYPT_BLOWFISH,$this->_key,$data,MCRYPT_MODE_CBC,ENCRYPTION_KEY);
        return $cipherText;
    }
    
    protected function _decrypt($data,$key)
    {
        $cipherText = mcrypt_decrypt(MCRYPT_BLOWFISH,$this->_key,$data,MCRYPT_MODE_CBC,ENCRYPTION_KEY);
        return $cipherText;
    }
    
    protected function _parseResponse($response)
    {
        if(preg_match('/([a-z]+)\s+"([^"]+)"/i',$response,$responseData)) {
            $this->_apiMessage = $responseData[2];
            return array('status' => strtolower($responseData[1]), 'message' => $responseData[2]);
        }
        return false;
    }
    
    protected function _curlGet($url)
    {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch,CURLOPT_HEADER,false);
        curl_setopt($ch,CURLOPT_HTTPGET,true);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,20);
        
        $rawData = curl_exec($ch);
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        
        if($http_code == 200)
            return $rawData;
        return false;
    }
}