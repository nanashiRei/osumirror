<?php

require dirname(__FILE__).'/../inc/twitteroauth/twitteroauth.php';

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_TOKEN, OAUTH_SECRET);
$content = $connection->get('account/verify_credentials');
function post_to_twitter($message)
{
    global $connection; 
    $connection->post('statuses/update', array('status' => $message));
}

?>