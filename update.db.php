#!/usr/bin/php5
<?php

if(!isset($_SERVER['_'])) die('CLI Script: Cannot be run from HTTP!');

error_reporting(E_ALL);

include 'inc/config.php';
include 'bashcolor.php';
include 'php/twitter.api.php';

$defThemeDir = dir('/mnt/store/osufiles/packs/default');
$filesPath = '/mnt/store/osufiles';
$mapsPath = '/mnt/store/osufiles/maps';

echo 'Reading default pack:', "\n";

/*** Read general packs ***/
while($defPack = $defThemeDir->read())
{
	if($defPack[0] == '.') continue;
	if(preg_match("/default_(\d+)\.rar/",$defPack,$packMatch))
	{
		$packres = $m->query("SELECT * FROM `beatmap_packs` WHERE `themeid` = 1 AND `packnum` = " . $packMatch[1]);
		if(!$packres->num_rows)
		{
			bashthis( '´wRunning ´gunrar ´wto extract new maps', "\n" );
			$NULL = `unrar e -o- '$filesPath/packs/default/$defPack' *.osz *.zip $mapsPath/ &> /dev/null`;
			
			bashthis( '´wReading ´g', $defPack, "\n" );
			
			$fileList = explode("\n",trim(`unrar lb '$filesPath/packs/default/$defPack' *.osz *.zip`));
			
			$m->query(sprintf("INSERT INTO `beatmap_packs` (`packnum`,`size`,`beatmaps`,`filename`,`uploader`) VALUES (%d,%d,%d,'%s','%s')",
                $packMatch[1],
                filesize($filesPath.'/packs/default/'.$defPack),
                count($fileList),
                $defPack,
                'Shizuku Sangou'));
            
			$packIdRes = $m->query(sprintf("SELECT `id` FROM `beatmap_packs` WHERE `packnum` = %s LIMIT 1",
                $packMatch[1]));
            
			$packIdArr = $packIdRes->fetch_assoc();
			
			$osuMapsList = array();
			
			foreach($fileList as $oszFile)
			{
				bashthis( '´g', $oszFile, '´w: ' );
				if(preg_match("/^(?<mapid>\d+) (?<map>.*)\.(osz|zip)/", $oszFile, $oszFileMatch))
				{
					bashthis( '´w[´Y#', $oszFileMatch['mapid'], '´w] ', $oszFileMatch['map'], "\n" );
					if(file_exists($mapsPath.'/'.$oszFile))
					{
						bashthis( '´gFound´w: maps/', $oszFile, ' [´Y', filesize($mapsPath.'/'.$oszFile), ' Bytes´w]' );
						$osuMapsList[] = sprintf("(%d,'%s','%s',%d,%d)",
							$oszFileMatch['mapid'],
							$m->real_escape_string($oszFileMatch['map']),
							$m->real_escape_string($oszFile),
							$packIdArr['id'],
							filesize($mapsPath.'/'.$oszFile)
						);
                        post_to_twitter('New Map: ' . $oszFileMatch['map'] . ' on http://osu.yas-online.net/m#' . $oszFileMatch['mapid'] . ' #osugame #beatmap');
					}
					else
					{
						bashthis( '´rNot found.' );
					}
				}
				echo "\n";
			}
			
            post_to_twitter('Beatmap Pack #' . $packMatch[1] . ' new on http://osu.yas-online.net/p#' . $packMatch[1] . ' #osugame #beatmappack');
            
			$m->query(sprintf("INSERT INTO `beatmap_maps` (`mapid`,`map`,`filename`,`packid`,`filesize`) VALUES %s",implode(',', $osuMapsList)));
			//debugging
			//die();
		}
	}
}


/*** Read themed packs ***/
$themedDir = dir('/mnt/store/osufiles/packs');

while($theme = $themedDir->read())
{
	if($theme[0] == '.' || $theme == 'default') continue;
	if(is_dir($filesPath.'/packs/'.$theme))
	{			
		$themedOszFiles = dir($filesPath.'/packs/'.$theme);
		
		bashthis( '´wBrowsing Theme "´g', $theme, "´w\"\n" );
		
		$themeRes = $m->query(sprintf("SELECT * FROM `beatmap_themes` WHERE `theme` = '%s' LIMIT 1",$m->real_escape_string($theme)));
		if(!$themeRes->num_rows)
		{
			bashthis( '´YNew theme has been added: ´g', $theme, "\n" );
			$m->query(sprintf("INSERT INTO `beatmap_themes` (`theme`) VALUES ('%s')",$m->real_escape_string($theme)));
			$themeIdRes = $m->query(sprintf("SELECT `id` FROM `beatmap_themes` WHERE `theme` = '%s'",$m->real_escape_string($theme)));
			$themeIdArr = $themeIdRes->fetch_assoc();
			$themeId = $themeIdArr['id'];
		}
		else 
		{
			$themeIdArr = $themeRes->fetch_assoc();
			$themeId = $themeIdArr['id'];
		}
		
		while($themedOszFile = $themedOszFiles->read())
		{
			if($themedOszFile[0] == '.') continue;
			if(preg_match("/(\d+)\.rar$/",$themedOszFile,$packMatch))
			{
				$packres = $m->query(sprintf("SELECT * FROM `beatmap_packs` WHERE `packnum` = %d AND `themeid` = %d", 
                    $packMatch[1],
                    $themeId));
                
				if(!$packres->num_rows)
				{
					bashthis( '´wRunning ´gunrar ´wto extract new theme', "\n" );
					$NULL = `unrar e -o- '$filesPath/packs/$theme/$themedOszFile' *.osz *.zip $mapsPath/ &> /dev/null`;
					
					bashthis( '´wReading ´g', $themedOszFile, "\n" );
					$fileList = explode("\n",trim(`unrar lb '$filesPath/packs/$theme/$themedOszFile' *.osz *.zip`));
					
					$m->query(sprintf("INSERT INTO `beatmap_packs` (`packnum`,`size`,`beatmaps`,`themeid`,`filename`,`uploader`) VALUES (%d,%d,%d,%d,'%s','%s')",
                        $packMatch[1],
                        filesize($filesPath.'/packs/'.$theme.'/'.$themedOszFile),
                        count($fileList),
                        $themeId,
                        $m->real_escape_string($themedOszFile),
                        'Shizuku Sangou'));
                    
					$packIdRes = $m->query(sprintf("SELECT `id` FROM `beatmap_packs` WHERE `packnum` = %s AND `themeid` = %d LIMIT 1",
                        $packMatch[1],
                        $themeId));
                    
					$packIdArr = $packIdRes->fetch_assoc();
					
					$osuMapsList = array();
					
					foreach($fileList as $oszFile)
					{
						bashthis( '´g', $oszFile, '´w: ' );
						if(preg_match("/^(?<mapid>\d+) (?<map>.*)\.(osz|zip)/", $oszFile, $oszFileMatch))
						{
							bashthis( '´w[´Y#', $oszFileMatch['mapid'], '´w] ', $oszFileMatch['map'], "\n" );
							if(file_exists($mapsPath.'/'.$oszFile))
							{
								bashthis( '´gFound´w: maps/', $oszFile, ' [´Y', filesize($mapsPath.'/'.$oszFile), ' Bytes´w]' );
								$osuMapsList[] = sprintf("(%d,'%s','%s',%d,%d)",
									$oszFileMatch['mapid'],
									$m->real_escape_string($oszFileMatch['map']),
									$m->real_escape_string($oszFile),
									$packIdArr['id'],
									filesize($mapsPath.'/'.$oszFile)
								);
                                post_to_twitter('New Map: ' . $oszFileMatch['map'] . ' on http://osu.yas-online.net/m#' . $oszFileMatch['mapid'] . ' #osugame #beatmap');
							}
							else
							{
								bashthis( '´rNot found.' );
							}
						}
						echo "\n";
					}
					
                    post_to_twitter($theme . ' Pack #' . $packMatch[1] . ' new on http://osu.yas-online.net/t#' . str_replace(' ','_',$theme) . '-' . $packMatch[1] . ' #osugame #themepack');
                    
					$m->query(sprintf("INSERT INTO `beatmap_maps` (`mapid`,`map`,`filename`,`packid`,`filesize`) VALUES %s",implode(',', $osuMapsList)));
				}	
			}
		}
	}
}

// * DISC USAGE * //
$dir = '/mnt/store/osufiles';
$usagePacks = explode("\t",`du -sb '$dir/packs'`);
$usageMaps = explode("\t",`du -sb '$dir/maps'`);

$m->query(sprintf('UPDATE `beatmap_stats` SET `diskusage_maps`=%d, `diskusage_packs`=%d',$usageMaps[0],$usagePacks[0]));

?>
