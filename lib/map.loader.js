function updatePackInfo(pack,text)
{
    if(pack.result == 'error')
    {
        
    }
    else
    {
        $('#packInfo').empty();
        
        var pd = pack.success;
        for(map in pd) break;
        
        $('div#packTitle strong').text(pd[map].mapid);
        $('div#packTitle #mapName').text(pd[map].map);
        $('span#packSize').html(Math.round(pd[map].filesize / 1024 / 1024)+' MB');
        $('#downloadButton').attr('href',pd[map].downloadLink);
        
        copybt.setText( 'Download (' + pd[map].map + ')[' + location.href + '] from the YaS-Online.net mirror!' );
        
        // Templating???
        $('#packInfoTemplate #downloadCount').text(pd[map].downloads);
        $('#packInfoTemplate #fileName').text(pd[map].filename);
        
        $('#packInfoTemplate table.maptable tbody').empty();
        var maps = 0;
        for(m in pd)
        {
            $('<tr><td id="' + pd[m].themeid + ':' + pd[m].theme + ':' + pd[m].packnum + '">#' + pd[m].packnum + '</td><td>' + pd[m].theme + '</td><td>' + Math.round(pd[m].size / 1024 / 1024) + ' MB</td></tr>').appendTo('#packInfoTemplate table.maptable tbody');
            maps++;
        }
        if(!maps)
        {
            $('<tr><td colspan="3">There seems to a problem with this list D:</td></tr>').appendTo('#packInfoTemplate table.maptable tbody');
        }
        $('table.maptable tbody tr:nth-child(even)').addClass('even');
        
        $('#packInfoTemplate table.maptable tbody tr').hover(function(){ $(this).addClass('hover'); }).mouseout(function(){ $(this).removeClass('hover') }).click(function(){
            var packData = $(this).children().first().attr('id').split(':');
            location.href = '/' + (packData[0] == 1 ? 'p' : 't') + '#' + (packData[0] == 1 ? packData[2] : packData[1] + '-' + packData[2]); 
        });
        
        $('#packInfoTemplate').children().clone(true).appendTo('#packInfo');
    }
}

function jumpNavHandle()
{
    var jumpNav = location.hash.substring(1);
    if(jumpNav)
    {  
        $('#packInfo').empty();
        $('<span><img src="/images/ajax-loader.gif" alt="loading"> Loading data...</span>').appendTo('#packInfo');
        $.getJSON('/json.mapdata.php',{mapId:jumpNav},updatePackInfo);
    }
    else
    {
        $('.contentbody').empty();
        $('<div id="errorMessage">' + maps_not_selected_error + '</div>').appendTo('.contentbody');
    }
}

$(document).ready(function(){
    
    copybt = new ZeroClipboard.Client();
    copybt.setHandCursor( true );
    copybt.glue( 'copyInGameButton'/*, 'copyTextCont'*/ );
    
    jumpNavHandle();
    
});