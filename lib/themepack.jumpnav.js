function subSectionOpen(e){
    $('.leftpanel a#subSectionButton').removeClass('activesection'); 
    $('div#packSection').hide(200); 
    $(e).addClass('activesection').next().show(200);
}

function updatePackInfo(pack,text)
{
    if(pack.result == 'error')
    {
        $('div#packTitle strong').text('ERR');
        $('div#packInfo').html(pack.error);
    }
    else
    {
        $('#packInfo').empty();
        
        var pd = pack.success;
        var packName = pd.theme + ' #' + pd.packnum + '.rar';
        $('div#packTitle strong').text(pd.packnum);
        $('div#packTitle #themeTitle').text(pd.theme);
        $('span#packSize').html(Math.round(pd.size / 1024 / 1024)+' MB');
        $('#downloadButton').attr('href',pd.downloadLink);
        
        copybt.setText( 'Download (' + packName + ')[' + location.href + '] from the YaS-Online.net mirror!' );
        
        // Templating???
        $('#packInfoTemplate #uploader').text(pd.uploader);
        $('#packInfoTemplate #downloadCount').text(pd.downloads);
        $('#packInfoTemplate #fileName').text(packName);
        
        $('#packInfoTemplate table.maptable tbody').empty();
        var maps = 0;
        for(map in pd.maps)
        {
            $('<tr><td id="' + pd.maps[map].mapid + '">#' + pd.maps[map].mapid + '</td><td>' + pd.maps[map].map + '</td><td>' + Math.round(pd.maps[map].filesize / 1024 / 1024) + ' MB</td></tr>').appendTo('#packInfoTemplate table.maptable tbody');
            maps++;
        }
        if(!maps)
        {
            $('<tr><td colspan="3">There seems to a problem with this list D:</td></tr>').appendTo('#packInfoTemplate table.maptable tbody');
        }
        $('table.maptable tbody tr:nth-child(even)').addClass('even');
        
        var qry = (location.search != '' ? location.search : ''); 
        $('#packInfoTemplate table.maptable tbody tr').hover(function(){ $(this).addClass('hover'); }).mouseout(function(){ $(this).removeClass('hover') }).click(function(){ location.href = '/m' + qry + '#' + $(this).children().first().attr('id'); });
        
        var dlStatus = (typeof pd.downloaded != 'undefined' && pd.downloaded == true ? beatmap_label_downloaded : beatmap_label_not_downloaded);
        $('#packInfoTemplate #downloadStatus').text(dlStatus);
        
        $('#packInfoTemplate').children().clone(true).appendTo('#packInfo');
        
        document.title = 'osu! Beatmap Packs :: ' + pd.theme + ' #' + pd.packnum;
    }
}

function jumpNavHandle()
{
    var jumpNav = location.hash.substring(1);
    if(jumpNav)
    {  
        $('#packSection a').removeClass('active');
        $('#packSection a[id="pack' + jumpNav + '"]').addClass('active');
        var themePack = jumpNav.split('-');
        $('#packInfo').empty();
        $('<span><img src="/images/ajax-loader.gif" alt="loading"> Loading data...</span>').appendTo('#packInfo');
        $.getJSON('/json.packdata.php',{themeId:themePack[0],packNum:themePack[1]},updatePackInfo);
    }
    else
    {
        location.hash = $('#packSection a').first().addClass('active').attr('id').substring(4);
        jumpNavHandle();
    }
}

$(document).ready(function(){
    
    copybt = new ZeroClipboard.Client();
    copybt.setHandCursor( true );
    copybt.glue( 'copyInGameButton'/*, 'copyTextCont'*/ );
    
    jumpNavHandle();
    
    $('#packSection a').click(function(){
        location.hash = $(this).attr('id').substring(4);
        jumpNavHandle();
    });

    //$('a#sectionButton:first-child').click().next().children('a#subSectionButton').first().click();
    $('#packSection a.active').parent().parent().parent().parent().prev().click();
    $('#packSection a.active').parent().parent().parent().prev().click();  
    
});