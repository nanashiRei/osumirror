function readDownloads(){
    $.getJSON('/json.downloads.php',{},function(dl){
        $('#downloads tbody').empty();
        for(i in dl){
            $('<tr>' +
                '<td>' + dl[i].id + '</td>' +
                '<td style="padding: 5px;"><div style="background: #' + dl[i].ip.substr(2) + '; width: 16px; height: 16px; border-radius: 4px; box-shadow: 0 0 5px #' + dl[i].ip.substr(2) + ';"></div></td>' +
                '<td>' + 
                    dl[i].device + ' - ' + dl[i].platform + 
                    '<br />' +
                    '<small>' + dl[i].ident.toLowerCase() + '</small>' +
                '</td>' +
                '<td>' + (dl[i].type == 1 ? 'Pack' : 'Map') + '</td>' +
                //'<td>' + dl[i].ident.toLowerCase() + '</td>' +
                //'<td><img src="/images/lang/' + dl[i].location + '.png" alt="flag" /></td>' +
                //'<td>' + dl[i].themeid + '</td>' + 
                '<td>' + 
                    dl[i].packid + 
                    '<br />' + 
                    '<small>' + dl[i].themeid + '</small>' +
                '</td>' +
                '<td>' + dl[i].size + '</td>' +
                '<td>' + dl[i].timestamp + '</td>' +
            '</tr>')
            .appendTo('#downloads tbody');
        };
        $('#downloads tbody tr:nth-child(even)').addClass('even');
    });
}

$(document).ready(function(){

    setInterval('readDownloads();',5000);
    readDownloads();

});