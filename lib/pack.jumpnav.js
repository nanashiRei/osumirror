function subSectionOpen(e){
    $('.leftpanel a#subSectionButton').removeClass('activesection'); 
    $('div#packSection').hide(200); 
    $(e).addClass('activesection').next().show(200);
}

function sectionOpen(e){
    $('.leftpanel a#sectionButton').removeClass('activesection'); 
    $('div#packBlockSection').hide(200); 
    $(e).addClass('activesection').next().show(200);
    $(e).next().children('a#subSectionButton').removeClass('activesection');
    $(e).next().children('div#packSection').hide(0);
}

function updatePackInfo(pack,text)
{
    if(pack.result == 'error')
    {
        $('div#packTitle strong').text('ERR');
        $('div#packInfo').html(pack.error);
    }
    else
    {
        $('#packInfo').empty();
        
        var pd = pack.success;
        var packName = beatmap_pack_default + ' #' + pd.packnum + '.rar';
        $('div#packTitle strong').text(pd.packnum);
        $('span#packSize').html(Math.round(pd.size / 1024 / 1024)+' MB');
        $('#downloadButton').attr('href',pd.downloadLink);
        
        copybt.setText( 'Download (' + packName.substring(0,packName.length-4) + ')[' + location.href + '] from the YaS-Online.net mirror!' );
        
        // Templating???
        $('#packInfoTemplate #uploader').text(pd.uploader);
        $('#packInfoTemplate #downloadCount').text(pd.downloads);
        $('#packInfoTemplate #fileName').text(packName);
        
        $('#packInfoTemplate table.maptable tbody').empty();
        var maps = 0;
        for(map in pd.maps)
        {
            $('<tr><td id="' + pd.maps[map].mapid + '">#' + pd.maps[map].mapid + '</td><td>' + pd.maps[map].map + '</td><td>' + Math.round(pd.maps[map].filesize / 1024 / 1024) + ' MB</td></tr>').appendTo('#packInfoTemplate table.maptable tbody');
            maps++;
        }
        if(!maps)
        {
            $('<tr><td colspan="3">There seems to a problem with this list D:</td></tr>').appendTo('#packInfoTemplate table.maptable tbody');
        }
        $('table.maptable tbody tr:nth-child(even)').addClass('even');
        
        var qry = (location.search != '' ? location.search : ''); 
        $('#packInfoTemplate table.maptable tbody tr')
        .hover(function(){ $(this).addClass('hover'); })
        .mouseout(function(){ $(this).removeClass('hover') })
        .click(function(){ location.href = '/m' + qry + '#' + $(this).children().first().attr('id'); });
        
        var dlStatus = (typeof pd.downloaded != 'undefined' && pd.downloaded == true ? beatmap_label_downloaded : beatmap_label_not_downloaded);
        $('#packInfoTemplate #downloadStatus').text(dlStatus);
        
        //$('#packInfoTemplate #twitterbutton').attr('data-url',location.href).attr('data-text','osu! ' + beatmap_pack_default + ' ' + pd.packnum);
        //<iframe allowtransparency="true" frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:130px; height:20px;"></iframe>
        
        $('#packInfoTemplate').children().clone(true).appendTo('#packInfo');
        
        /*
        $('<iframe/>')
        .attr('allowtransparency',true)
        .css({width:130,height:20})
        .attr('frameborder',0)
        .attr('scrolling','no')
        .attr('src','//platform.twitter.com/widgets/tweet_button.html?text=' + escape('osu! ' + beatmap_pack_default + ' ' + pd.packnum) + '&url=' + escape('http://osupacks.ppy.sh/p/'+pd.packnum) + '&hashtags=osugame')
        .appendTo('#packInfo #twitterbutton');
        */
        
        document.title = 'osu! Beatmap Packs :: ' + pd.theme + ' #' + pd.packnum;
    }
}

function jumpNavHandle()
{
    var jumpNav = location.hash.substring(1);
    if(jumpNav)
    {  
        $('#packSection a').removeClass('active');
        $('#packSection a[id="pack' + jumpNav + '"]').addClass('active');
        $('#packInfo').empty();
        $('<span><img src="/images/ajax-loader.gif" alt="loading"> Loading data...</span>').appendTo('#packInfo');
        $.getJSON('/json.packdata.php',{themeId:1,packNum:jumpNav},updatePackInfo);
    }
    else
    {
        location.hash = $('#packSection a').first().addClass('active').attr('id').substring(4);
        jumpNavHandle();
    }
}

$(document).ready(function(){
    
    copybt = new ZeroClipboard.Client();
    copybt.setHandCursor( true );
    copybt.glue( 'copyInGameButton'/*, 'copyTextCont'*/ );
    
    jumpNavHandle();
    
    $('#packSection a').click(function(){
    location.hash = $(this).attr('id').substring(4);
        jumpNavHandle();
    });

    //$('a#sectionButton:first-child').click().next().children('a#subSectionButton').first().click();
    $('#packSection a.active').parent().parent().parent().parent().prev().click();
    $('#packSection a.active').parent().parent().parent().prev().click();  
    
});