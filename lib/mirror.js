
function update_stats_h(t){
    var stats = $('.headline .stats');
    stats.empty();
    stats.html(
        '<b>Monthly Stats</b> - Mirror ver. 1.6.0<br />' +
        '[ Traffic <b>Maps</b> @' + t.traffic.maps + ' - <b>Packs</b> @' + t.traffic.packs + ' ] ' +
        '[ <b>' + t.downloads + '</b> downloads ]' +
        '<br />[ Disc usage <b>Maps</b> @' + t.usage.maps + ' - <b>Packs</b> @' + t.usage.packs + ' ]'
    );
}

function update_stats(){
    $.getJSON('/json.traffic.php',{},update_stats_h);
}

function choose_theme(style){
    $('#page_stylesheet').attr('href','/css/' + style);
    $.cookie('style',style,{expires: 7});
    $('.themepicker a').removeClass('selected').css('boxShadow','none');
    $('.themepicker a#themed_' + style).addClass('selected').css('boxShadow','0 0 7px ' + $('.themepicker #themed_' + style).css('backgroundColor'));
    $('.themepicker a:not(.selected)').animate({'width': 15},{duration: 150, queue: false});
    $('.themepicker a#themed_' + style).parent().prependTo('.themepicker ul');
}

function update_headline_bg(){
    if($('.headline').css('backgroundColor') != $('.headline_bg').css('backgroundColor'))
    {
        $('.headline_bg').css({
            'backgroundColor': $('.headline').css('backgroundColor'),
            'borderColor': $('.navigation').css('backgroundColor'),
            'boxShadow': '0 0 35px ' + $('.navigation').css('backgroundColor')
        });
    }
}

/* Document Ready Events */
$(document).ready(function(){
    ZeroClipboard.setMoviePath('/zc/ZeroClipboard.swf');
    var ua = $.browser;

    $('.languages img').click(function(){ location.href = location.pathname + '?l=' + $(this).attr('alt') + location.hash });
    $('.languages').fadeTo(500,0.4)
    .hover(
        function(){ $(this).stop().fadeTo(500,1); },
        function(){ $(this).stop().fadeTo(500,0.4); }
    );
    
    var theme = $.cookie('style');
    $('.themepicker #themed_' + theme).addClass('selected').css('boxShadow','0 0 7px ' + $('.themepicker #themed_' + theme).css('backgroundColor')).parent().prependTo('.themepicker ul');
    $('.themepicker a').hover(
        function(){
            if($(this).hasClass('selected')) return;
            $(this).animate({'width': 20},{duration: 150, queue: false});
        },
        function(){
            if($(this).hasClass('selected')) return;
            $(this).animate({'width': 15},{duration: 150, queue: false});
        }
    );

    update_headline_bg();
    setInterval("update_headline_bg();",50);
    
    setInterval("$('p.stats a').fadeOut(250).fadeIn(250);",1000);
    
    var link = location.pathname.substring(1);
    if(link){ $('.navigation a[href="/'+link+'"]').addClass('active'); }
    var qry = location.search.substring(1);
    if(qry){ $('.navigation a').attr('href',function(){ return $(this).attr('href') + '?' + qry; }); }
    update_stats();
    setInterval("update_stats();",10000);
});

$(window).load(function(){
    var e = document.createElement("script"); 
    e.setAttribute("type", "text/javascript"); 
    e.setAttribute("src", "http://browser-update.org/update.js"); 
    document.body.appendChild(e);
});

/* Google Custom Fonts */
WebFontConfig = {
google: { families: [ 'Open Sans:400,300:latin,cyrillic,latin-ext,greek'/*,'Terminal Dosis Light', 'Patrick Hand'*/ ] }
};

/* Google Analytics */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-23242468-3']);
_gaq.push(['_trackPageview']);

/* Get Clicky */
var clicky_site_ids = clicky_site_ids || [];
clicky_site_ids.push(66474058);

(function() {
    // Fonts
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
    
    // Analytics
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    
    // Clicky Stats
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = '//static.getclicky.com/js';
    ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( s );
})();