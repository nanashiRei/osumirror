function deleteElement()
{
    $(this).remove();
}

function handleSearchResult(result,text)
{
    $('.contentbody div#errorMessage').each(function(i,e){ $(this).fadeOut(150 + (i * 10),deleteElement); });
    $('.contentbody a#searchResult').remove();
    // TODO: JSON PHP Part
    if(result.result == 'success')
    {
        $('input#searchQueryString').val($('input#searchQueryString').val().toLowerCase());
        var r = result.success;
        var i = 0;
        var qry = (location.search != '' ? location.search : ''); 
        for(k in r)
        {
            $('<a href="/m' + qry + '#' + r[k].mapid + '" id="searchResult">' + r[k].map + '</a>').appendTo('.contentbody').hide(0).fadeIn(350 + (i * 50));
            if(r[k].pack) $('<a href="/' + (r[k].pack.themeid > 1 ? 't' : 'p') + qry + '#' + (r[k].pack.themeid != 1 ? r[k].pack.theme.replace(' ','_') + '-' : '' ) + r[k].pack.packnum + '" id="searchResult" class="packresult">' + r[k].pack.theme + ' #' + r[k].pack.packnum + '</a>').appendTo('.contentbody').hide(0).fadeIn(150 + (i * 10));;
            i++;
        }
    }
    else
    {
        $('<div id="errorMessage">Server says: ' + result.error + '</div>').appendTo('.contentbody');
    }
}

lastsearch = '';
function doSearch()
{
    var query = location.hash.substring(1);
    
    if(lastsearch == query) return;
    
    if(query.length >= 3)
    {
        lastsearch = query;
        $.getJSON('/json.search.php',{searchQuery:query.replace(/\+/g,' ')},handleSearchResult);
        document.title = 'osu! Beatmap Packs :: "' + query.replace(/\+/g,' ') + '"';
    }
    else
    {
        $('.contentbody div#errorMessage').each(function(i,e){ $(this).fadeOut(150 + (i * 10),deleteElement); });
        $('.contentbody a#searchResult').remove();
        $('<div id="errorMessage">The search query is to short</div>').appendTo('.contentbody');
    }
}

$(document).ready(function(){
    var query = location.hash.substring(1);
    if(query)
    {
        $('input#searchQueryString').val(query.replace(/\+/g,' '));
        doSearch();
    }
    
    $('#searchBar form#searchForm').submit(function(e){ e.preventDefault(); }).children().first().keyup(function(){
        var search = $(this).val().toLowerCase();
        
        //location.href = '/f#' + search.replace(/ /g,'+');
        if(typeof searchTimer != 'undefined') clearTimeout(searchTimer);
        searchTimer = setTimeout('location.href = "/f#' + search.replace(/ /g,'+') + '"; doSearch();',500);
    });
    
    $('#searchQueryString').focus();
});