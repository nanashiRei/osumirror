<?php

if(!isset($_SERVER['_'])) exit;

include dirname(__FILE__).'/../inc/config.php';
include dirname(__FILE__).'/../inc/api_config.php';
include dirname(__FILE__).'/../inc/encryption.php';
include dirname(__FILE__).'/../php/twitter.api.php';

error_reporting(E_ALL & ~E_NOTICE);
header('content-type: text/plain');

$rsync_cmd = 'rsync -rlptv --rsh="ssh -p %d -i %s" %s/* %s@%s:~/';
$rsync_cmd_excl = 'rsync -rlptv --exclude-from=filelist.tmp --size-only --delete --delete-excluded --force --rsh="ssh -p %d -i %s" %s/maps/ %s@%s:~/maps/';
$remote_cmd = 'ssh -i %s -p %d %s@%s ';

$where = '1';
if(is_numeric($argv[1]))
    $where = '`id` = ' . intval($argv[1]);

$res = $m->query("SELECT * FROM `beatmap_mirrors` WHERE $where");
$mirrors = array();
while($mr = $res->fetch_assoc()) $mirrors[$mr['id']] = $mr;

define('LF',chr(13).chr(10));

chdir(dirname(__FILE__));

foreach($mirrors as $mirror)
{
    $ip = ($mirror['ssh_override_ip'] > 0 ? long2ip($mirror['ssh_override_ip']) : gethostbyname($mirror['host'])); 
    echo 'Selecting "', $mirror['host'], '" [', $ip, ']', LF;
    echo 'Speed: ', sprintf('%0.2f MB/s',$mirror['network_speed'] / 8), LF;
    echo '-------------------------------------------------------------------------------------',LF;
    if(!$mirror['ssh_sync'])
    {
        echo 'Skipping SYNC of "', $mirror['host'], '" because `ssh_sync` is disabled.', LF;
    }
    else
    {
        echo 'Starting Sync process using rsync...', LF;
        if(@fsockopen($ip,$mirror['ssh_port'],$e,$n,5))
        {
            $keyfile = './keys/'.$mirror['host'].'.key';
            if(file_exists($keyfile))
            {
                $remote = sprintf($remote_cmd,$keyfile,$mirror['ssh_port'],$mirror['ssh_user'],$ip);
                $hdd_usage = intval(`$remote du -sm`);
                $hdd_percent = (1 / ($mirror['hdd_quota'] / 1024)) * $hdd_usage;
                echo $hdd_usage, ' MB used, ', ($mirror['hdd_quota'] != -1 ? ($mirror['hdd_quota'] / 1024) . ' MB is limit (' . sprintf('%0.2f%%',100*$hdd_percent) . ')' : 'there is no limit.'), LF;
                // if($mirror['hdd_quota'] > 0 && $hdd_percent > .95)
                // {
                    // echo 'Cleaning up HDD...', LF;
                    // $_t = `$remote "find ./maps -type f -printf '%T@ %p\n' | sort -k 1 -n | head -n 20 | sed 's/^[^ ]* //' | xargs --delimiter='\n' rm"`;
                // }
                if($mirror['hdd_quota'] > 0)
                {
                    echo 'Building maps transfer list...', LF;
                    $res = $m->query("SELECT * FROM `beatmap_maps` WHERE 1 ORDER BY `added` DESC, `mapid` DESC");
                    @unlink('filelist.tmp');
                    $excl_file = fopen('filelist.tmp','w');
                    $quota = $mirror['hdd_quota'] * 1024;
                    $stacksize = 0;
                    $delete_itemids = array();
                    $insert_itemids = array();
                    while($map = $res->fetch_assoc())
                    {
                        $map_store = $m->query("SELECT * FROM `beatmap_stores` WHERE `type` = 2 AND `itemid` = " . $map['mapid'] . " AND `dbid` = " . $map['id'] . " AND `mirrorid` = " . $mirror['id'] . " LIMIT 1");
                        if($stacksize < $quota)
                        {
                            fputs($excl_file,'+ ' . $map['filename'] . chr(10));
                            $stacksize += $map['filesize'];
                            if(!$map_store->num_rows)
                                $insert_itemids[] = array($map['mapid'],$map['id']);
                        }
                        else
                        {
                            if($map_store->num_rows)
                                $delete_itemids[] = $map['mapid'].'/'.$map['id'];                            
                        }
                    }
                    if(count($delete_itemids) > 0)
                    {
                        echo 'Deleting ', count($delete_itemids), ' maps from the stores table... ';
                        $query = "DELETE FROM `beatmap_stores` WHERE `type` = 2 AND CONCAT(`itemid`,'/',`dbid`) IN (" . implode(',',$delete_itemids) . ") AND `mirrorid` = " . $mirror['id'];
                        //echo 'Query: ', $query, LF;
                        if($m->query($query))
                            echo 'OK';
                        else
                            echo 'FAILED';
                        echo LF;
                    }
                    if(count($insert_itemids) > 0)
                    {
                        echo 'Adding ', count($insert_itemids), ' maps to the stores table... ';
                        $insert_queries = array();
                        foreach($insert_itemids as $item)
                            $insert_queries[] = sprintf('(%d,%d,%d,%d)',$mirror['id'],2,$item[0],$item[1]);
                        $query = "INSERT INTO `beatmap_stores` (`mirrorid`,`type`,`itemid`,`dbid`) VALUES ". implode(',',$insert_queries);
                        //echo 'Query: ', $query, LF;
                        if($m->query($query))
                            echo 'OK';
                        else
                            echo 'FAILED';
                        echo LF;
                    }
                    fputs($excl_file,'- *'.chr(10));
                    fclose($excl_file);
                    echo 'Transfer list build. Size is ', floor($stacksize/pow(1024,2)), 'M', LF;
                    $cmd = sprintf($rsync_cmd_excl,$mirror['ssh_port'],$keyfile,OSUFILES_PATH,$mirror['ssh_user'],$ip);
                }
                else
                {
                    $cmd = sprintf($rsync_cmd,$mirror['ssh_port'],$keyfile,OSUFILES_PATH,$mirror['ssh_user'],$ip);
                }
                //echo 'Exec: ', $cmd, LF;
                passthru($cmd);
            }
            else
            {
                echo 'Error `', $keyfile, '` could not be found. Aborting.', LF;
            }
        }
        else
        {
            echo 'Host ', $ip, ' timed out. Skipping... (5 sec)', LF;
        }
    }
    
    $manual = (is_numeric($argv[1]) ? '[Manual] ' : '[Auto]');
    post_to_twitter($manual . 'Syncing of '. $mirror['host'] . ' finished. #osumirror #osugame');
    
    echo '-- done -----------------------------------------------------------------------------',LF;
    echo LF;
}