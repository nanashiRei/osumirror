<?php

include 'inc/config.php';

$query = "
	SELECT 
		`beatmap_packs`.*,
		`beatmap_themes`.`theme`
	FROM 
		`beatmap_packs`,
		`beatmap_themes`
	WHERE
		`beatmap_packs`.`themeid` = `beatmap_themes`.`id` 
	ORDER BY
		`beatmap_themes`.`id` ASC, 
		`beatmap_packs`.`packnum` ASC";
        
$res = $m->query($query);
if($res->num_rows)
{
    $packages = array();
    while($row = $res->fetch_assoc())
    {
        $packages[$row['theme']][$row['packnum']] = $row;
    } 
    echo json_encode(array('result'=>'success','success'=>$packages));
}
else
{
    echo json_encode(array('result'=>'error','error'=>'Cannot get a list of packages!'));
}

?>