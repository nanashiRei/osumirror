<?php

include 'inc/config.php';

// * TRAFFIC * //
date_default_timezone_set('Europe/Berlin');
$start_time = mktime(0,0,0,date('m'),1,date('Y'));
$stats = $m->query('SELECT * FROM `beatmap_stats` WHERE `year`=YEAR(NOW()) AND `month`=MONTH(NOW())')->fetch_object();

function F($s){
    $i=0;
    while($s > 1000){$s /= 1024; $i++;}
    $f=array('B','KB','MB','GB','TB');
    return sprintf('%0.2f %s',$s,$f[$i]);
}

echo json_encode(array(
    'traffic' => array(
        'maps' => F($stats->traffic_maps),
        'packs' => F($stats->traffic_packs)
    ),
    'downloads' => ($stats->downloads_maps + $stats->downloads_packs),
    'usage' => array(
        'maps' => F($stats->diskusage_maps),
        'packs' => F($stats->diskusage_packs)
    )
));
exit;