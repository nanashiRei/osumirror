<?php

include 'inc/config.php';

$query = "
	SELECT 
		`beatmap_packs`.*,
		`beatmap_themes`.`theme` 
	FROM 
		`beatmap_packs`,
		`beatmap_themes`
	WHERE
		`beatmap_packs`.`themeid` = `beatmap_themes`.`id`
        AND `beatmap_packs`.`themeid` = 1
	ORDER BY
		`beatmap_themes`.`id` ASC, 
		`beatmap_packs`.`packnum` DESC";

$packListRes = $m->query($query);

$packList = array();
while($pack = $packListRes->fetch_assoc())
{
    $packList[$pack['packnum'] - $pack['packnum'] % 100][$pack['packnum']] = $pack;
}

$smrt->assign('packList',$packList);
$smrt->assign('UserDL',$userDL);
$smrt->display('packList.tpl');

?>