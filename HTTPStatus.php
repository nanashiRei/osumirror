<?php
/************************
*** HTTP STATUS *********
************************/

if(!isset($_REQUEST['error']))
    die();

if(!isset($smrt))
    require 'php/smarty.inc.php';

$smrt->display('http_status/'.$_REQUEST['error'].'.tpl');

?>
