<?php

include 'inc/config.php';
include 'inc/api_config.php';
include 'inc/encryption.php';

header('content-type: text/html');

$module = (empty($_GET['module']) ? 'livedownloads' : $_GET['module']);
$mfile = 'stats_'.$module.'.php';

if(isset($_COOKIE['auth']) && $_COOKIE['auth'] == hash_hmac('sha1',$_SERVER['REMOTE_ADDR'],'osubm-admin'))
{
    if(file_exists($mfile))
    {
        include $mfile;
    }
    else
    {
        $_REQUEST['error'] = 404;
        header('HTTP/1.1 404 File not Found');
        include 'HTTPStatus.php';
    }
}
else
{
    $smrt->display('admin_auth.tpl');
}