<?php

include 'inc/config.php';

if(empty($_GET['searchQuery']) || strlen($_GET['searchQuery']) < 3)
    die(json_encode(array('result'=>'error','error'=>'Search query to short!')));

$query = "
	SELECT 
		`beatmap_packs`.*,
		`beatmap_themes`.`theme` 
	FROM 
		`beatmap_packs`,
		`beatmap_themes`
	WHERE
		`beatmap_packs`.`themeid` = `beatmap_themes`.`id`
        AND `beatmap_packs`.`id` = ";
    
$searchQuery = str_replace(' ','%',$_GET['searchQuery']);
$mapRes = $m->query("SELECT * FROM `beatmap_maps` WHERE `map` LIKE '%" . $m->real_escape_string($searchQuery) . "%' ORDER BY `map` ASC LIMIT 50");

if($mapRes->num_rows)
{
    $result = array();
    while($map = $mapRes->fetch_assoc())
    {
        $result[$map['mapid']] = $map;
        $packRes = $m->query($query . $map['packid']);
        if($packRes->num_rows)
            $result[$map['mapid']]['pack'] = $packRes->fetch_assoc();
    }
    die(json_encode(array('result'=>'success','success'=>$result)));
}
else
{
    die(json_encode(array('result'=>'error','error'=>'Nothing could be found :(')));
}

?>