<?php

header('content-type: text/plain');

function encrypt($data,$key)
{
    $cipherText = mcrypt_encrypt(MCRYPT_BLOWFISH,$key,$data,MCRYPT_MODE_CBC,ENCRYPTION_KEY);
    return $cipherText;
}

function decrypt($data,$key)
{   
    $cipherText = mcrypt_decrypt(MCRYPT_BLOWFISH,$key,$data,MCRYPT_MODE_CBC,ENCRYPTION_KEY);
    return $cipherText;
}