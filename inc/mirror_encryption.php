<?php

header('content-type: text/plain');

function encrypt($data)
{
    $cipherText = mcrypt_encrypt(MCRYPT_BLOWFISH,ENCRYPTION_SECRET,$data,MCRYPT_MODE_CBC,ENCRYPTION_KEY);
    return $cipherText;
}

function decrypt($data)
{   
    $cipherText = mcrypt_decrypt(MCRYPT_BLOWFISH,ENCRYPTION_SECRET,$data,MCRYPT_MODE_CBC,ENCRYPTION_KEY);
    return $cipherText;
}