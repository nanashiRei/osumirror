<?php

class FileLog
{
    private $name;
    private $counter;
    private $splitSize;
    private $fileName;
    private $absoluteFileName;
    
    private $fileHandle;
    
    /** Log Types **/
    const LogInfo = 'INFO';
    const LogWarn = 'WARNING';
    const LogError = 'ERROR';
    const LogCritical = 'CRITICAL';
    const LogDebug = 'DEBUG';
   
    /**
     * Contructor
     */
    public function __construct($logname='log',$splitAtBytes=-1)
    {
        $this->name = $logname;
        $this->counter = 1;
        $this->splitSize = $splitAtBytes;
        
        if(!file_exists(LOG_DIR))
            mkdir(LOG_DIR,755);
            
        $this->makeFileName();  
    }
    
    /** Public Stuff **/
    public function add($text,$type=FileLog::LogInfo)
    {
        $line = sprintf('%s [%s] %s%s',strftime('%H:%M:%S'),$type,$text,chr(10));
        fputs($this->fileHandle,$line,strlen($line));
    }
    
    /** Private Stuff **/
    private function openLog()
    {
        if($this->fileHandle) fclose($this->fileHandle);
        $this->fileHandle = fopen($this->absoluteFileName,'a');
    }
    
    private function makeFileName()
    {
        $this->fileName = sprintf('%s_%s.%d.log',$this->name,strftime('%Y-%m-%d'),$this->counter);
        $this->absoluteFileName = LOG_DIR.'/'.$this->fileName;
        $this->verifyFileName();
    }
    
    private function verifyFileName()
    {
        if(!file_exists($this->absoluteFileName) && !file_exists($this->absoluteFileName.'.gz'))
        {
            touch($this->absoluteFileName);
            file_put_contents(
                $this->absoluteFileName,
                str_pad('>- '. __CLASS__ .' file created ['.strftime('%Y/%m/%d - %H:%M:%S').'] -<',80,'#',STR_PAD_BOTH) . chr(10)
            );
        }
        
        $this->openLog();
        
        if($this->splitSize > 0 && (file_exists($this->absoluteFileName.'.gz') || filesize($this->absoluteFileName) > $this->splitSize))
        {
            $this->counter++;
            if(!file_exists($this->absoluteFileName.'.gz'))
            {
                if(flock($this->fileHandle,LOCK_EX))
                {
                    $this->add('End of log. Running gzip to save space on disk.');
                    exec('gzip -f '.$this->absoluteFileName.' &');
                    flock($this->fileHandle,LOCK_UN);
                }
            }
            else
            {
                if(file_exists($this->absoluteFileName)) unlink($this->absoluteFileName);
            }
            $this->makeFileName();
        }
    }
}