<?php

function verify_ident($remote_ident)
{
    $key = file_get_contents(dirname(__FILE__).'/osudroid.key');
    $local_ident = hash_hmac('sha1',$_SERVER['REMOTE_ADDR'],$key);
    return ($local_ident == $remote_ident);
}

function get_ident()
{
    return $_GET['device_id'];
}

?>