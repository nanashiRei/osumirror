<?php

// BAN SYSTEM
require dirname(__FILE__).'/blacklist.php';

error_reporting(E_ALL & ~E_NOTICE);
//error_reporting(E_ALL);
header('Cache-Control: no-store');
session_start();

define('MYSQL_HOST',	'localhost');
define('MYSQL_USER',	'osubm');
define('MYSQL_PASS',	'ws7vPTmHaXj66LLB');
define('MYSQL_DB',		'osubm');

define('DOWNLOAD_SALT', 'a9sudj3o9wl45ubno963unlg8o5uzn3bki8lz5n8klszn35hzb');
define('DLCOOKIE',      'osu-beatmap-download');

define('DEBUG_MODE',    false);
define('LOG_FILE',      'select_' . strftime('%Y-%d-%m') . '.log');
define('LOG_DIR',       dirname(__FILE__).'/../log');

define("CONSUMER_KEY",  "dfZhS724aoFIq6N3mpY8g");
define("CONSUMER_SECRET",   "oj0BoVvc8TSMnbxYXFJeQd4ZJlj0VYPrGPsMZcZzPU");
define("OAUTH_TOKEN",   "491729585-xD8UQ8lld2lsjQic4FMP97vncxOoBoJXcKJsbSxU");
define("OAUTH_SECRET",  "h7MMf1f4gUVQ8ksBfyv2baI3Lcet5ibh80ab6l6j4");

define('ENCRYPTION_KEY', 'osumiroR');

date_default_timezone_set('Europe/Berlin');

require dirname(__FILE__).'/../php/smarty.inc.php';
require dirname(__FILE__).'/logger.php';

//$log_access = new FileLog('access',10*pow(1024,2));
//$log_access->add(sprintf('"%s" "%s" (%s) %s',$_SERVER['HTTP_HOST'],$_SERVER['REMOTE_ADDR'],$_SERVER['HTTP_USER_AGENT'],$_SERVER['REQUEST_URI']));

function resetCookie(){
    setcookie(DLCOOKIE,bin2hex(gzdeflate(serialize(array()),9)),time()+365*24*60*60,'/',$_SERVER['HTTP_HOST']);
    $_COOKIE[DLCOOKIE] = serialize(array());
}

function decodeCookie(){
    return unserialize(gzinflate(@pack('H*',$_COOKIE[DLCOOKIE])));
}

function saveCookie($c){
    $deflatedData = bin2hex(gzdeflate(serialize($c),9));
    $_COOKIE[DLCOOKIE] = $deflatedData;
    setcookie(DLCOOKIE,$deflatedData,time()+365*24*60*60,'/',$_SERVER['HTTP_HOST']);
}

function addDownload($theme,$packnum){
    $c = decodeCookie();
    $c[$theme][$packnum] = true;
    saveCookie($c);
}

if(!isset($_SERVER['SHELL']))
{
    if(!isset($_COOKIE[DLCOOKIE]))
        resetCookie();
    
    if(!preg_match('/[a-f0-9]+/i',$_COOKIE[DLCOOKIE]))
        resetCookie();
        
    $userDL = decodeCookie();
}

if(DEBUG_MODE)
{
    if(isset($_GET['override_style']))
    {
        $_COOKIE['style'] = $_GET['override_style'];
        setcookie('style',$_GET['override_style'],time()+(60*60));
    }

    if(isset($_GET['clear_style']))
        unset($_COOKIE['style']);
}

if(!isset($_COOKIE['style']) || preg_match('{(style|\.css$)}i',$_COOKIE['style']))
{
    $files = glob('style/*.css');
    $rand = mt_rand(0,250000) % count($files);
    $style = $files[$rand];
    $_COOKIE['style'] = basename(substr($style,0,-4));
    setcookie('style',$_COOKIE['style'],time()+(3*60));
}

if(isset($_POST['_auth_me']) && isset($_POST['passkey']))
{
    $_COOKIE['auth'] = hash_hmac('sha1',$_SERVER['REMOTE_ADDR'],$_POST['passkey']);
    setcookie('auth',$_COOKIE['auth']);
}

// Neico: Added Persistent Connection in hope to keep the mysql server from overflowing
$m = new mysqli('p:' . MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);

?>