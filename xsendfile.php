<?php

include 'inc/config.php';
include 'inc/encryption.php';
//var_dump($_GET);

$localHash = hash('sha1',ip2long($_SERVER['REMOTE_ADDR']).DOWNLOAD_SALT);
$localPath = '/mnt/store/osufiles';
//$localPath = '.';

ini_set('display_errors',true);
error_reporting(E_ALL);

$log = new FileLog('select',10*pow(1024,2));

function select_mirror()
{
    global $m, $log, $download_data;
    //$res = $m->query("SELECT `id`,`host`,`network_traffic`,`key` FROM `beatmap_mirrors` WHERE `active` = 1 ORDER BY `network_traffic` ASC, `network_hits` ASC, `network_speed` DESC");
    $res = $m->query("SELECT * FROM `beatmap_mirrors` WHERE `active` = 1");
    $mirrors = array();
    $mids = array();
    while($mr = $res->fetch_assoc())
    {
        //fputs($f,'Mirror #' . $mr['id'] . ' trying to select...' . chr(10));
        $type = ($_GET['downloadType'] == 'pack' ? 1 : 2);
        $itemid = intval(($type == 1 ? $_GET['packNum'] : $_GET['mapId']));
        $dbid = $download_data['database_id'];
        if($mr['partial'] == false || $m->query("SELECT * FROM `beatmap_stores` WHERE `type` = ". $type . " AND `mirrorid` = " . $mr['id'] . " AND `itemid` = " . $itemid . " AND `dbid` = " . $dbid . " LIMIT 1")->num_rows)
        {
            $mirrors[] = $mr;
            $mids[] = $mr['id'];
            //fputs($f,'Selected ' . $mr['id'] . '/' . $mr['host'] . ' for ' . $type . ':' . $itemid . chr(10));
        }
    }
    $mkey = mt_rand(0,99999999999) % count($mirrors);
    $mirror = $mirrors[$mkey];
    $log->add('['.$_SERVER['REMOTE_ADDR'].'] ('.implode('|',$mids).') Selected #'.$mirror['id'].'/'.$mirror['host'].' for '.$type.':'.$itemid,FileLog::LogInfo);
    $m->query("UPDATE `beatmap_mirrors` SET `network_hits` = `network_hits` + 1 WHERE `id` = " . $mirror['id'] . " LIMIT 1");
    return $mirror;
}

include dirname(__FILE__).'/php/downloadFunctions.php';
include dirname(__FILE__).'/php/V2.php';

function F($s){
    $i=0;
    while($s > 1000){$s /= 1024; $i++;}
    $f=array('B','KB','MB','GB','TB');
    return sprintf('%0.2f %s',$s,$f[$i]);
}

if(empty($_GET['downloadType']))
{
    header('Location: /');
    die();
}

$device = "PC";
if(!empty($_GET['device']))
{
    $device = $_GET['device'];
}

$platform = 'PC';
if(!empty($_GET['platform']))
{
    $platform = $_GET['platform'];
}

if(preg_match('#android#i',$_SERVER['HTTP_USER_AGENT']))
{
    $deive = 'android';
}

$user_ident = sprintf('%X',ip2long($_SERVER['REMOTE_ADDR']));

if($device == 'android')
{
    $platform_file = 'inc/platform_'.$_GET['platform'].'.php';
    if(file_exists($platform_file))
    {
        include $platform_file;
        $user_ident = get_ident();
        if(empty($_GET['ident']) || !verify_ident($_GET['ident']))
        {
            header('HTTP/1.1 401 Unauthorized');
            header('Content-Type: text/plain');
            die('ERROR 401 Unauthorized');
        }
    }
    else
    {
        header('HTTP/1.1 400 Bad Request');
        header('Content-Type: text/plain');
        die('ERROR 400 Unsupported platform [' . $_GET['platform'] .']');
    }
}

if($device != 'PC')
{
    $lip = ip2long($_SERVER['REMOTE_ADDR']);
    $eident = $m->real_escape_string($user_ident);
    $r = $m->query("SELECT SUM(`size`) as downloaded FROM `beatmap_downloads` WHERE (`ip` = $lip || `ident` =  '$eident') AND `timestamp` > UNIX_TIMESTAMP() - 300 ORDER BY `timestamp` DESC");
    $dl_traffic = $r->fetch_object()->downloaded;
    if($dl_traffic > (20 * pow(1024,2)))
    {
        header('HTTP/1.1 509 Bandwidth Limit Exceeded');
        header('Content-Type: text/plain');
        die('ERROR 509 Bandwidth Limit Excceeded [' . F($dl_traffic) . ' in last 5 minutes]');
    }
}
else
{
    $lip = ip2long($_SERVER['REMOTE_ADDR']);
    $r = $m->query("SELECT SUM(`size`) as downloaded FROM `beatmap_downloads` WHERE `ip` = $lip AND `timestamp` > UNIX_TIMESTAMP() - 1800 ORDER BY `timestamp` DESC");
    $dl_traffic = $r->fetch_object()->downloaded;
    if($dl_traffic > (1000 * pow(1024,2)))
    {
        header('HTTP/1.1 509 Bandwidth Limit Exceeded',true,509);
        header('Content-Type: text/html');
        $_REQUEST['error'] = 509;
        $_REQUEST['downloaded'] = F($dl_traffic);
        include dirname(__FILE__).'/HTTPStatus.php';
        die();
    }
}

$mime_type = 'application/octet-stream';
switch($_GET['downloadType'])
{
    case 'pack':
        $mine_type = 'application/x-rar-compressed';
        if(!empty($_GET['themeId']) && !empty($_GET['packNum']))
        {
            $packDataRes = $m->query("
                SELECT `beatmap_packs`.*, `beatmap_themes`.`theme` 
                FROM `beatmap_packs`,`beatmap_themes` 
                WHERE 
                    `beatmap_themes`.`id` = `beatmap_packs`.`themeid` 
                    AND `packnum` = " . $_GET['packNum'] . " 
                    AND `themeid` = " . $_GET['themeId'] . " 
                LIMIT 1");
            if($packDataRes->num_rows)
            {
                $packData = $packDataRes->fetch_assoc();
                if($localHash == $_GET['hash'])
                {
                    $themeDir = ($packData['themeid'] == 1 ? 'default' : $packData['theme']);
                    $absolutePath = $localPath.'/packs/'.$themeDir.'/'.$packData['filename'];
                    if(!file_exists($absolutePath))
                    {
                        header('HTTP/1.1 404 File not found');
                        $smrt->display('http_status/404.tpl');
                        exit;
                    }
                    else
                    {
                        $dlEntry = $m->query(sprintf("SELECT * FROM `beatmap_downloads` WHERE `type` = 1 AND `ip` = %d AND `packid` = %d AND `themeid` = %d AND `timestamp` > UNIX_TIMESTAMP() - 1800",
                            ip2long($_SERVER['REMOTE_ADDR']),
                            $packData['packnum'],
                            $packData['themeid']
                        ));
                        if(!$dlEntry->num_rows)
                        {
                            $m->query("UPDATE `beatmap_packs` SET `downloads` = `downloads` + 1 WHERE `id` = " . $packData['id']);
                            $m->query(sprintf("INSERT INTO `beatmap_downloads` (`type`,`packid`,`themeid`,`timestamp`,`size`,`device`,`platform`,`ip`,`ident`) VALUES (1,%d,%d,UNIX_TIMESTAMP(),%d,'%s','%s',%d,'%s')",
                                $packData['packnum'],
                                $packData['themeid'],
                                $packData['size'],
                                $m->real_escape_string($device),
                                $m->real_escape_string($platform),
                                ip2long($_SERVER['REMOTE_ADDR']),
                                $user_ident));
                        }   
                        addDownload($packData['theme'],$packData['packnum']);
                        $filename = $packData['theme'] . ' #' . $packData['packnum'] . '.rar';
                        
                        $download_data = array(
                            'file_type' => 'pack',
                            'theme' => $packData['theme'],
                            'pack_num' => $packData['packnum'],
                            'database_id' => $packData['id'],
                            'filename' => $packData['filename'],
                            'filesize' => $packData['size'],
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'timestamp' => time()
                        );  
                    }
                }
                else
                {
                    header('Location: /' . ($packData['themeid'] == 1 ? 'p#' : 't#') . ($packData['themeid'] == 1 ? $packData['packnum'] : $packData['theme'] . '-' . $packData['packnum']));
                    exit;
                }
            }
            else
            {
                header('HTTP/1.1 404 File not found');
                $smrt->display('http_status/404.tpl');
                exit;
            }
        }
        else
        {
            header('Location: /');
            exit;
        }
        break;
    case 'map':
        $mine_type = 'application/x-zip-compressed';
        if(!empty($_GET['mapId']))
        {
            $mapRes = $m->query("SELECT * FROM `beatmap_maps` WHERE `mapid` = " . $_GET['mapId'] . " ORDER BY `id` DESC LIMIT 1");
            if($mapRes->num_rows)
            {
                $mapData = $mapRes->fetch_assoc();
                if($localHash == $_GET['hash'])
                {
                    $absolutePath = $localPath.'/maps/'.$mapData['filename'];
                    if(!file_exists($absolutePath))
                    {
                        header('HTTP/1.1 404 File not found');
                        $smrt->display('http_status/404.tpl');
                        exit;
                    }
                    else
                    {
                        $dlEntry = $m->query(sprintf("SELECT * FROM `beatmap_downloads` WHERE `type` = 2 AND `ip` = %d AND `packid` = %d AND `themeid` = %d AND `timestamp` > UNIX_TIMESTAMP() - 1800",
                            ip2long($_SERVER['REMOTE_ADDR']),
                            $mapData['id'],
                            $mapData['mapid']
                        ));
                        if(!$dlEntry->num_rows)
                        {
                            $m->query("UPDATE `beatmap_maps` SET `downloads` = `downloads` + 1 WHERE `id` = " . $mapData['id']);
                                
                            $m->query(sprintf("INSERT INTO `beatmap_downloads` (`type`,`packid`,`themeid`,`timestamp`,`size`,`device`,`platform`,`ip`,`ident`) VALUES (2,%d,%d,UNIX_TIMESTAMP(),%d,'%s','%s',%d,'%s')",
                                $mapData['mapid'],
                                $mapData['id'],
                                $mapData['filesize'],
                                $m->real_escape_string($device),
                                $m->real_escape_string($platform),
                                ip2long($_SERVER['REMOTE_ADDR']),
                                $user_ident));
                        }
                        $filename = $mapData['mapid'] . ' - ' . $mapData['map'] . '.osz';
                        
                        $download_data = array(
                            'file_type' => 'map',
                            'filename' => $mapData['filename'],
                            'database_id' => $mapData['id'],
                            'filesize' => $mapData['filesize'],
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'timestamp' => time()
                        );
                    }
                }
                else
                {
                    header('Location: /m#' . $mapData['mapid']);
                    exit;
                }
            }
            else
            {
                header('HTTP/1.1 404 File not found');
                $smrt->display('http_status/404.tpl');
                exit;
            }
        }
        else
        {
            header('Location: /');
            exit;
        }
        break;
}

$m->query(sprintf("DELETE FROM `beatmap_downloads` WHERE `timestamp` <= %d",mktime(0,0,0,date('m'),date('d'),date('Y')) - (14 * 24 * 60 * 60)));

if(empty($absolutePath))
{
    header('HTTP/1.1 403 Forbidden');
    $smrt->display('http_status/403.tpl');
    exit;
}

if(isset($download_data))
{
    $mirror = select_mirror();
    $tries = 10;
    if($mirror['version'] == 2)
        $v2 = new OsuMirror_Api_V2($mirror['host'], $mirror['key']);

    $m->query("UPDATE `beatmap_mirrors` SET `network_traffic` = `network_traffic` + " . $download_data['filesize'] . " WHERE `id` = " . $mirror['id'] . " LIMIT 1");
    
    $download_data['timestamp'] = time();
    $log->add('Download: '.floor($download_data['filesize']/1024/1024).'M "'.$download_data['filename'].'" from '.$mirror['host'],FileLog::LogInfo);
    
    $url = '';
    if($mirror['version'] == 1) {
        $download_data = bin2hex(encrypt(json_encode($download_data),$mirror['key']));
        $url = 'http://' . $mirror['host'] . '/api/get_download/?request=' . $download_data;
    } else {
        $data = $v2->makeTicket($download_data);
        $url = 'http://' . $mirror['host'] . '/api/download/' . $data;
    }
    
    header('location: ' . $url);
    die('Download: ' . $url);
}

?>