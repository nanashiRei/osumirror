<?php

include 'inc/config.php';

if(empty($_GET['mapId']))
    die(json_encode(array('result'=>'error','error'=>'Missing arguments')));
    
$mapListRes = $m->query("
        SELECT 
            `beatmap_maps`.*, 
            `beatmap_themes`.`theme`, 
            `beatmap_packs`.`packnum`,
            `beatmap_packs`.`themeid`,
            `beatmap_packs`.`size`
        FROM 
            `beatmap_maps`,
            `beatmap_packs`,
            `beatmap_themes` 
        WHERE `beatmap_maps`.`packid` = `beatmap_packs`.`id` AND 
            `beatmap_themes`.`id` = `beatmap_packs`.`themeid` AND
            `beatmap_maps`.`mapid` = " . $_GET['mapId'] . "
        ORDER BY
            `beatmap_maps`.`packid` DESC");

$mapList = array();

if($mapListRes->num_rows)
{
    $dlHash = hash('sha1',ip2long($_SERVER['REMOTE_ADDR']).DOWNLOAD_SALT);
    while($map = $mapListRes->fetch_assoc())
    {
        $map['downloadLink'] = '/fetch/'.$dlHash.'-m-'.$map['mapid'].'.osz';
        $mapList[$map['id']] = $map;
    }
    die(json_encode(array('result'=>'success','success'=>$mapList)));
}
else
{
    die(json_encode(array('result'=>'error','error'=>'MapID #' . $_GET['mapId'] . ' could not be found in the database.')));
}

?>