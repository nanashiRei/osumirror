<?php

$res = $m->query('SELECT * FROM `beatmap_mirrors` ORDER BY `network_traffic` DESC, `network_hits` DESC');
$mirrors = array();
while($mr = $res->fetch_assoc())
{
    $mirrors[$mr['id']] = $mr;
    $h=fsockopen($mr['host'],80,$errno,$errstr,4);
    $mirrors[$mr['id']]['online'] = ($h ? true : false);
    fclose($h);
}

//$smrt->assign('Mirrors',parse_ini_file(dirname(__FILE__).'/mirror_sync/mirrors.ini'));
$smrt->assign('Mirrors',$mirrors);
$smrt->display('mirrors.tpl');