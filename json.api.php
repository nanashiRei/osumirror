<?php

header('content-type: text/plain');
include 'inc/api_config.php';

function api($d,$ok=true)
{
    $o = array();
    $o['result'] = ($ok ? 'success' : 'error');
    $o[$o['result']] = $d;
    return json_encode( $o );
}

if(!empty($_GET['func']) && file_exists('apicall/'.$_GET['func'].'.php'))
{
    include 'apicall/' . $_GET['func'] . '.php';
    if(!isset($_GET['args']) || !is_array($_GET['args'])) $_GET['args'] = array();
    @call_user_func_array($_GET['func'],$_GET['args']);
}
else
{
    echo '{"result":"error","error":"no such api call"}';
}