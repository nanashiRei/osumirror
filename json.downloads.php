<?php

include 'inc/config.php';

$r = $m->query("SELECT * FROM `beatmap_downloads` ORDER BY `timestamp` DESC LIMIT 50");

function F($s){
    $i=0;
    while($s > 1000){$s /= 1024; $i++;}
    $f=array('B','KB','MB','GB','TB');
    return sprintf('%0.2f %s',$s,$f[$i]);
}

$downloads = array();
while($dl = $r->fetch_assoc())
{
    $IP = $dl['ip'];
    $dl['ip'] = hash('crc32',DOWNLOAD_SALT.long2ip($dl['ip']));
    $dl['timestamp'] = date('H:i.s - d-m-Y',$dl['timestamp']);
    $dl['size'] = F($dl['size']);
    //$dl['location'] = strtolower(geoip_country_code_by_name($IP));
    //if(empty($dl['location']) || $dl['location'] == 'ap') $dl['location'] = 'error';
    $downloads[] = $dl;
}

echo json_encode($downloads);
exit;

?>