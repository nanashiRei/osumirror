<?php

require 'inc/mirror_encryption.php';

function get_theme($t)
{
    if(empty($t) || $t == 'Beatmap Pack')
        return 'default';
    return $t;
}

function get_download()
{
    if(!empty($_GET['request']) && !preg_match('/[^a-f0-9]/i',$_GET['request']))
    {
        $file_data_raw = trim(decrypt(pack('H*',$_GET['request'])));
        $file_data = @json_decode($file_data_raw);
        if(isset($file_data->file_type))
        {
            if($file_data->ip == $_SERVER['REMOTE_ADDR'])
            {
                if(time() - $file_data->timestamp <= 60)
                {
                    switch($file_data->file_type)
                    {
                        case 'pack':
                            $abspath = OSUFILES_PATH . '/packs';
                            $abspath .= '/'.get_theme($file_data->theme);
                            $abspath .= '/'.$file_data->filename;
                            if(file_exists($abspath))
                            {
                                header('content-type: application/x-rar-compressed');
                                header('content-disposition: filename="' . $file_data->theme . ' #' . $file_data->pack_num . '.rar"');
                                header('content-length: ' . filesize($abspath));
                                header('x-sendfile: ' . $abspath);
                            }
                            else
                            {
                                echo api('file could not be found',false);
                            }
                            break;
                        case 'map':
                            $abspath = OSUFILES_PATH . '/maps/';
                            $abspath .= $file_data->filename;
                            if(file_exists($abspath))
                            {
                                header('content-type: application/x-zip-compressed');
                                header('content-disposition: filename="' . $file_data->filename . '"');
                                header('content-length: ' . filesize($abspath));
                                header('x-sendfile: ' . $abspath);
                            }
                            else
                            {
                                echo api('file could not be found. ' . $file_data->filename,false);
                            }
                            break;
                        default:
                            echo api('no type was supplied',false);
                            break;
                    }
                }
                else
                {
                    echo api('download timed out',false);
                }
            }
            else
            {
                echo api('ip missmatch in request',false);
            }
        }
        else
        {
            echo api('invalid file_data',false);
        }
    }
    else
    {
        echo api('invalid input',false);
    }
}