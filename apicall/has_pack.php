<?php

function has_pack()
{
    $fargv = func_get_args();
    
    $theme = (!empty($_REQUEST['theme']) ? $_REQUEST['theme'] : 'default');
    
    $packs = array();
    for($i=0;$i<func_num_args();$i++)
    {
        if(file_exists(OSUFILES_PATH.'/packs/'.$theme.'/'.$fargv[$i]))
        {
            $packs[$fargv[$i]] = true;
        }
        else
        {
            $packs[$fargv[$i]] = false;
        }
    }   
    echo api( $packs );
}