<?php

$packsRes = $m->query("
    SELECT 
		`beatmap_packs`.*,
		`beatmap_themes`.`theme`
	FROM 
		`beatmap_packs`,
		`beatmap_themes`
	WHERE
		`beatmap_packs`.`themeid` = `beatmap_themes`.`id`
		AND `beatmap_packs`.`themeid` = 1
	ORDER BY
		`beatmap_packs`.`packnum` DESC");

$mirrorsRes = $m->query("
    SELECT *
    FROM
        `beatmap_mirrors`
    ORDER BY
        `host` ASC");

$packs = array();    
while($pack = $packsRes->fetch_assoc())
{  
    $packs[$pack['packnum']] = array(
        'file_type' => 'pack',
        'theme' => $pack['theme'],
        'pack_num' => $pack['packnum'],
        'database_id' => $pack['id'],
        'filename' => $pack['filename'],
        'ip' => $_SERVER['REMOTE_ADDR'],
        'timestamp' => time()
    ); 
}
krsort($packs);

$mirrors = array();
while($mirror = $mirrorsRes->fetch_assoc())
{
    $mirrors[$mirror['id']] = $mirror;
}

reset($packs);
reset($mirrors);

if(empty($_POST['mirror'])) $_POST['mirror'] = $mirrors[key($mirrors)]['id'];
if(empty($_POST['packnum'])) $_POST['packnum'] = $packs[key($packs)]['pack_num'];

$smrt->assign('BeatmapPacks', $packs);
$smrt->assign('Mirrors', $mirrors);
$smrt->assign('SelectedMirror', $mirrors[$_POST['mirror']]['host']);

$data = json_encode($packs[$_POST['packnum']]);
$smrt->assign('data',bin2hex(encrypt($data,$mirrors[$_POST['mirror']]['key'])));
$smrt->assign('deflated_data',bin2hex(encrypt(gzdeflate($data,9),$mirrors[$_POST['mirror']]['key'])));
$smrt->display('dltester.tpl');